'use strict';

import {PrettyMap} from "../src/pretty.map";
import {Pair} from "../src/pair";

describe('PrettyMap', () => {
    it('should create new PrettyMap from array', () => {
        let k: string = 'param_name';
        let v: string = 'param_value';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(1);
        expect(map.pairs[0].key).toEqual('param_name');
        expect(map.pairs[0].val).toEqual('param_value');
    });

    it('should create new PrettyMap from IPair', () => {
        let k: string = 'param_name';
        let v: string = 'param_value';
        const map: PrettyMap<string, string> = new PrettyMap<string, string>([
            new Pair<string, string>(k, v)
        ]);
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(1);
        expect(map.pairs[0].key).toEqual('param_name');
        expect(map.pairs[0].val).toEqual('param_value');
    });


    it('should set a Pair', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(2);
        expect(map.pairs[0].key).toEqual('param_name1');
        expect(map.pairs[1].key).toEqual('param_name2');
        expect(map.pairs[0].val).toEqual('param_value1');
        expect(map.pairs[1].val).toEqual('param_value2');
    });

    it('should set a Pair, then a second in a chain', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(3);
        expect(map.pairs[0].key).toEqual('param_name1');
        expect(map.pairs[1].key).toEqual('param_name2');
        expect(map.pairs[2].key).toEqual('param_name3');
        expect(map.pairs[0].val).toEqual('param_value1');
        expect(map.pairs[1].val).toEqual('param_value2');
        expect(map.pairs[2].val).toEqual('param_value3');
    });

    it('should set a Pair, but replace the second in a chain when second key is the same as the first', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name1', 'param_value3');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(2);
        expect(map.pairs[0].key).toEqual('param_name1');
        expect(map.pairs[1].key).toEqual('param_name2');
        expect(map.pairs[0].val).toEqual('param_value3');
        expect(map.pairs[1].val).toEqual('param_value2');
    });

    it('should delete the whole map', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        map.clear();
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(0);
    });

    it('should delete the second pair', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        map.delete('param_name2');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(2);
        expect(map.pairs[0].key).toEqual('param_name1');
        expect(map.pairs[1].key).toEqual('param_name3');
        expect(map.pairs[0].val).toEqual('param_value1');
        expect(map.pairs[1].val).toEqual('param_value3');
    });

    it('should find the value from the second pair', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        const value = map.get('param_name2');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(3);
        expect(value).toBeTruthy();
        expect(value).toEqual('param_value2');
    });

    it('should find the second pair', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        const exists = map.has('param_name2');
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(3);
        expect(exists).toBeTruthy();
    });

    it('should find three keys', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        const keys = map.keys();
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(3);
        expect(keys.length).toEqual(3);
    });

    it('should find three values', () => {
        let k: string = 'param_name1';
        let v: string = 'param_value1';
        const map: PrettyMap<string, string> = new PrettyMap([[k, v]]);
        map.set('param_name2', 'param_value2').set('param_name3', 'param_value3');
        const values = map.values();
        expect(map).toBeTruthy();
        expect(map.pairs.length).toEqual(3);
        expect(values.length).toEqual(3);
    });
});

