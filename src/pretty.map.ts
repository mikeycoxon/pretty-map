import { IPair, Pair } from './pair';

/**
 * An attempt on an improvement to TsMap so that JSON.stringify works better.
 * Currently, the JsonString representation of a TsMap looks like this:
 *
 *  "mappedParams":{
 *    "keyStore":[
 *      "firstKey",
 *      "secondKey"
 *    ],
 *    "valueStore":[
 *      "firstValue",
 *      "secondValue"
 *    ],
 *    "size":2
 * }
 * This is not very helpful, when needing that representation to make logical sense in APIs etc.
 *
 * what would be much better...
 *
 *  "mappedParams":{
 *    "entries":[
 *      {
 *        "key" : "firstKey",
 *        "val", "firstValue"
 *      },
 *      {
 *        "key" : "secondKey",
 *        "val", "secondValue"
 *      }
 *    ]
 *  }
 *  So, with this implementation, we are trying to get that while retaining the other conveniences of Maps
 */
export interface IPrettyMap<K, V> {
  set(k: K, v: V): any;

  get(k: K): V | undefined;

  has(k: K): boolean;

  delete(k: K): boolean;

  clear(): void;

  keys(): Array<K | undefined>;

  values(): Array<V | undefined>;

  forEach(cb: (value?: V, key?: K, map?: any) => void, context?: any): void;
}

export class PrettyMap<K, V> implements IPrettyMap<K, V> {
  public pairs: Array<IPair<K, V>> = [];

  // Accept an optional parameter,
  // The parameter's type:
  // [
  //   [K, V], [K, V], ...
  // ]
  constructor(items?: Array<[K, V]> | Array<Pair<K, V>>) {
    if (items) {
      for (const item of items) {
        if (item instanceof Pair) {
          this.pairs.push(item);
        } else {
          this.pairs.push(new Pair<K, V>(item[0], item[1]));
        }
      }
    } else {
      this.pairs = [];
    }
  }

  // set a key-value to Map,
  // return this to chain called.
  public set(k: K, v: V): IPrettyMap<K, V> {
    let exists = false;
    const ks = this.keys();

    // if key exists, replace it.
    for (let i = ks.length - 1; i >= 0; i--) {
      if (ks[i] === k) {
        this.pairs[i].val = v;
        exists = true;
      }
    }

    if (!exists) {
      this.pairs.push(new Pair<K, V>(k, v));
    }

    return this as IPrettyMap<K, V>;
  }

  // Return the value of the corresponding key,
  // if key not found, return undefind.
  public get(k: K): V | undefined {
    const ks: Array<K | undefined> = this.keys();
    for (let i = ks.length; i > -1; i--) {
      if (ks[i] === k) {
        return this.values()[i];
      }
    }

    return undefined;
  }

  /**
   * Determines whether a {@link Pair} lies within the map
   * @param k
   * @return true if a {@link Pair} with this key exists, otherwise false.
   */
  public has(k: K): boolean {
    const ks: Array<K | undefined> = this.keys();
    for (let i = ks.length; i > -1; i--) {
      if (ks[i] === k) {
        return true;
      }
    }
    return false;
  }

  /**
   * Delete the key and its corresponding value.
   * @param k the key of the {@link Pair} to be deleted.
   * @return true if the delete is successful, otherwise, false
   */
  public delete(k: K): boolean {
    const ks: Array<K | undefined> = this.keys();
    let len: number = ks.length;
    let deleteFlag = false;
    while (len--) {
      if (ks[len] === k) {
        this.pairs.splice(len, 1);
        deleteFlag = true;
      }
    }

    return deleteFlag;
  }

  /**
   * Empies the entire map.
   */
  public clear(): void {
    this.pairs.splice(0, this.pairs.length);
  }

  /**
   * Return all of the map's keys
   */
  public keys(): Array<K | undefined> {
    return this.pairs.map(e => e.key);
  }

  /**
   * Return all of the map's values
   */
  public values(): Array<V | undefined> {
    return this.pairs.map(e => e.val);
  }

  // Traverse the Map,
  // Accept two parameters, first is a callback, second is a optional context.
  // callback function accepts 3 optional params.
  // first is value, second is key, last is the map
  public forEach(cb: (value?: V, key?: K, map?: PrettyMap<K, V>) => void, context?: any): void {
    const ks: Array<K | undefined> = this.keys();
    const vs: Array<V | undefined> = this.values();
    for (let i = 0; i < this.pairs.length; i++) {
      cb.bind(context || this)(vs[i], ks[i], this);
    }
  }

  // implement toJSON to remove '"pairs": ' but not the value
}
