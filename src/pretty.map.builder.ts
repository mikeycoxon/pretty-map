import { IPair, Pair } from './pair';
import { PrettyMap } from './pretty.map';

export class PrettyMapBuilder<K, V> implements Partial<PrettyMap<K, V>> {
  public pairs: Array<IPair<K, V>> = [];

  public addPair(key: K, value: V): this & Pick<PrettyMap<K, V>, 'pairs'> {
    if (this.pairs) {
      this.pairs.push(new Pair<K, V>(key, value));
      return this;
    }
    return Object.assign(this, {
      pairs: [new PrettyMap<K, V>([new Pair<K, V>(key, value)])],
    });
  }

  // Accept an optional parameter,
  // The parameter's type:
  // [
  //   [K, V], [K, V], ...
  // ]
  public build(this: PrettyMap<K, V>) {
    return new PrettyMap([
      ...this.pairs.map(e => {
        return [e.key, e.val] as [K, V];
      }),
    ]);
  }
}
