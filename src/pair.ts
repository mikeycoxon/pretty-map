export interface IPair<K, V> {
  key?: K;
  val?: V;
}

export class Pair<K, V> implements IPair<K, V> {
  public key: K;
  public val: V;

  constructor(key: K, val: V) {
    this.key = key;
    this.val = val;
  }
}
