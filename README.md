# pretty-map
A typescript (and thus js-compatible) map interface.
It's heavily based on [ts-map](https://www.npmjs.com/package/ts-map), but structurally differs, because it is actually 
a simple array of Pairs with an interface that makes it behave like a map.
The motivation for this implementation is a prettier JSON stringify result.

## Installation
```sh
npm i @largesnike/pretty-map
```
## JSON.stringify Result
As mentioned above, what motivated this implementation is getting a simpler, more usable `JSON.stringify()` output.
Instead of getting:

```json
"mappedParams" : {
  "keyStore":[
    "firstKey",
    "secondKey"
  ],
  "valueStore":[
    "firstValue",
    "secondValue"
  ],
  "size":2
}

```
with this implementation, you will get:
```json
"mappedParams":{
  "pairs":[
     {
        "key":"firstKey",
        "val":"firstValue"
     },
     {
        "key":"secondKey",
        "val":"secondValue"
     },
  ]
}

```
This output is more helpful if you need to transport models between application layers, that is; use the JSON format
as a representation to be interpreted by another service.
## Usage
```typescript
import {PrettyMap} from '@largesnike/pretty-map';
```
### Generics
PrettyMap supports generics so that:
```typescript
interface Cat {
    name: string;
}

const map: new PrettyMap<number, Cat>([
    [1, {name: 'Sylvester'}]
])

map.set(2, {name: 'Tom'}) // no issue here

map.set(3, 'Felix') // runtime error, although Felix is a perfectly good name for a cat
```
### API
#### Constructor
As you have seen above, PrettyMap can be constructed with a nested Array:
```typescript
const map: new PrettyMap<string, string>([
    ['keyString1', 'valueString1'],
    ['keyString2', 'valueString2']
])
```
or it can be constructed as an array of Pairs:
```typescript
const map: PrettyMap<string, string> = new PrettyMap<string, string>([
    new Pair<string, string>({key: 'keyString1',val: 'valueString1'}),
    new Pair<string, string>({key: 'keyString2',val: 'valueString2'})
]);

```

#### length: number
PrettyMap, having an Array of pairs, exposes the length property, so you can return the map's size with:
```typescript
map.pairs.length;
```
#### set(k: K, v: V): PrettyMap<K, V>
Sets a key-value to a map, and it supports chained calling
```typescript
map.set('keyString1', 235);
map.set('keyString2', 140).set('keyString3', 300);
```
#### get(k: K): V | undefined
Returns the corresponding value of the supplied key. If the key is not present, the method returns undefined.
```typescript
map.set('keyString1', 235);
map.get('keyString1'); // 235
map.get('keyString2'); // undefined
```
#### has(k: K): boolean
Returns true if the key is present, otherwise false.
```typescript
map.set('keyString1', 235);
map.has('keyString1'); // true
map.has('keyString2'); // false
```
#### delete(k: k): boolean
Removes a key and its corresponding value from the map.
```typescript
map.set('keyString1', 235);
map.set('keyString2', 140);
map.delete('keyString1');
map.has('keyString1'); // false
map.pairs.length; // 1
```
#### clear(): void
Deletes all keys and values from the map.
```typescript
map.set('keyString1', 235);
map.set('keyString2', 140);
map.pairs.length; // 2
map.clear();
map.pairs.length; // 0
```
#### keys(): K[]
Returns all of the map's keys.
```typescript
map.set(1, 2);
map.set(true, false);
map.set(["1"], {name: 'mike'});
map.keys() // [1, true, ["1"]];
```
#### values(): K[]
Returns all of the map's values.
```typescript
map.set(1, 2);
map.set(true, false);
map.set(["1"], {name: 'mike'});
map.values() // [2, false, 'mike'];
```
